-- LibDateTime
--
-- LibDateTime provide locale date and time, formatting function, clock callback and more DateTime functions. This
-- library is localized in English, French, German, Spanish and Italian.
--
-- `DateTime' is a table in following format (or nil if not initialized) and support <, >, <=, >= and == operators:
-- {
--   year = 2008,
--   month = 9,
--   day = 12,
--   hour = 12,
--   minute = 0,
--   second = 0
-- }
-- 
-- INSTALL
--
-- You can add LibDateTime to dependencies in .mod file of your addon and tell user to install it from curse client or
-- manualy. But if you want you can include this library in your addon, in this case, add "EA_ChatWindow" in dependencies
-- in your .mod file, move LibDateTIme in lib folder and load all following files in .mod file (you can rename language files
-- or include content in yours files if you want) :
--
--     <File name="lib/LibStub.lua" />
--     <File name="lib/AceLocale-3.0.lua" />
--     <File name="lang/enEN.lua" />
--     <File name="lang/frFR.lua" />
--     <File name="lang/deDE.lua" />
--     <File name="lang/esES.lua" />
--     <File name="lang/itIT.lua" />
--     <File name="lib/LibDateTime.lua" />
--
-- In all case, load LibDateTime in your .lua file with adding this line :
--
--     local LibDateTime = LibStub("LibDateTime-0.4")
--
local MAJOR, MINOR = "LibDateTime-0.4", 7
local LibDateTime, oldminor = LibStub:NewLibrary(MAJOR, MINOR)

if not LibDateTime then return end -- No upgrade needed

-- Lib
local T = LibStub("WAR-AceLocale-3.0"):GetLocale("LibDateTime", true)

-- Jan, Feb, Mar, Apr, May, Jun, Jul, Aug, Sep, Oct, Nov, Dec
LibDateTime.DAYS_PER_MONTH =  {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}
LibDateTime.DAYS_PER_MONTH_LEAP =  {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}
LibDateTime.CUMULATED_DAYS_FOR_MONTH = {0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334}
LibDateTime.CUMULATED_DAYS_FOR_MONTH_LEAP = {0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335}

-- Full month names, in English.  Months count from 1 to 12; a month's numerical 
-- representation indexed into this table gives the name of that month.
LibDateTime.MONTHNAMES = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"}

-- Full names of days of the week, in English.  Days of the week count from 1 to 7; 
-- a day's numerical representation indexed into this array gives the name of that day.
LibDateTime.DAYNAMES = {"Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"}

-- Abbreviated month names, in English.
LibDateTime.MONTHNAMES_ABBR = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"}

-- Abbreviated day names, in English.
LibDateTime.DAYNAMES_ABBR = {"Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"}

-- Number of days in a year
LibDateTime.DAYS_PER_YEAR = 365
LibDateTime.DAYS_PER_YEAR_LEAP = 366

-- Number of seconds in a day, hour and minute
LibDateTime.SECONDS_PER_DAY = 86400
LibDateTime.SECONDS_PER_HOUR = 3600
LibDateTime.SECONDS_PER_MINUTE = 60

-- 
LibDateTime.startedSince = LibDateTime.startedSince or 0			-- Time elapsed from library starting
LibDateTime.elapsed = LibDateTime.elapsed or 0						-- Elapsed time counter

LibDateTime.currentDateTime = LibDateTime.currentDateTime or nil	-- Current DateTime table
LibDateTime.registredClock = LibDateTime.registredClock or {}		-- Array of clock register to receive time update

-- MetaTable for DateTime table with more functions
--
local DateTimeMetaTable = {

-- ==
-- 
__eq = function (a, b)
	return (a.year == b.year and a.month == b.month and a.day == b.day and a.hour == b.hour and a.minute == b.minute and a.second == b.second)
end,

-- <
-- 
__lt = function (a, b)
	if (a.year > b.year) then
		return false
	elseif (a.month > b.month) then
		return false
	elseif (a.day > b.day) then
		return false
	elseif (a.hour > b.hour) then
		return false
	elseif (a.minute > b.minute) then
		return false
	elseif (a.second >= b.second) then
		return false
	end

	return true
end,

-- <=
-- 
__le = function (a, b)
	return (a < b or a == b)
end,

-- tostring
--
__tostring = function (datetime)
	return string.format("DateTime: %04d-%02d-%02d %02d:%02d:%02d", datetime.year, datetime.month, datetime.day, datetime.hour, datetime.minute, datetime.second)
end,

}

-- pcall safecall implementation (we don't have loadstring to make dispatchers, so xpcall is more trouble than it's worth)
local pcall = pcall
local function errorchecker(success, ...)
	if not success then
        d(...)
        return nil
    else
        return ...
    end
end
 
local function safecall(func, ...)
	return errorchecker(pcall(func, ...))
end

-- Update `DateTime' every second.
--
function LibDateTimeOnUpdateDONOTTOUCH(elapsed)
	LibDateTime.elapsed = LibDateTime.elapsed + elapsed
	LibDateTime.startedSince = LibDateTime.startedSince + 1
	
	-- Initialize DateTime from chat entry and calendar data the first time
	if (not LibDateTime:IsInitialized()) then
		local index = TextLogGetNumEntries("Chat") - 1
		if (index >= 0) then
			local hour, minute, second = wstring.match(TextLogGetEntry("Chat", index), L"([0-9]+):([0-9]+):([0-9]+)")
			
			LibDateTime.currentDateTime = LibDateTime:New(
				tonumber(Calendar.todaysYear), 
				tonumber(Calendar.todaysMonth), 
				tonumber(Calendar.todaysDay), 
				tonumber(hour), tonumber(minute), 
				tonumber(second)
			)
			
			d("DateTime is initialized: "..LibDateTime:Format(LibDateTime.currentDateTime))
		end
	-- Update DateTime if already initialized every 1 second
	elseif(LibDateTime.elapsed >= 1) then
    while (LibDateTime.elapsed >= 1) do
      LibDateTime.elapsed = LibDateTime.elapsed - 1
      
      -- Increment second
      LibDateTime.currentDateTime.second = LibDateTime.currentDateTime.second + 1
      
      -- Increment minute after 60 seconds
      if (LibDateTime.currentDateTime.second >= 60) then
        LibDateTime.currentDateTime.second = 0
        LibDateTime.currentDateTime.minute = LibDateTime.currentDateTime.minute + 1
        
        -- Increment hour after 60 minutes
        if (LibDateTime.currentDateTime.minute >= 60) then
          LibDateTime.currentDateTime.minute = 0
          LibDateTime.currentDateTime.hour = LibDateTime.currentDateTime.hour + 1
          
          -- Increment day after 24 hours
          if (LibDateTime.currentDateTime.hour >= 24) then
            LibDateTime.currentDateTime.hour = 0
            LibDateTime.currentDateTime.day = LibDateTime.currentDateTime.day + 1
            
            -- Increment month after number of days for current month
            if ((LibDateTime:IsLeapYear(LibDateTime.currentDateTime.year) == true and LibDateTime.currentDateTime.day  > LibDateTime.DAYS_PER_MONTH_LEAP[LibDateTime.currentDateTime.month])
             or (LibDateTime:IsLeapYear(LibDateTime.currentDateTime.year) ~= true and LibDateTime.currentDateTime.day  > LibDateTime.DAYS_PER_MONTH[LibDateTime.currentDateTime.month])) then
              LibDateTime.currentDateTime.day = 1
              LibDateTime.currentDateTime.month = LibDateTime.currentDateTime.month + 1
              
              -- Incement year after 12 months
              if (LibDateTime.currentDateTime.month > 12) then
                LibDateTime.currentDateTime.month = 1
                LibDateTime.currentDateTime.year = LibDateTime.currentDateTime.year + 1
              end
            end
          end
        end
      end
    end
    
		-- Call registred clock callback
		table.foreach(LibDateTime.registredClock,
			function(handle, clock)
				safecall(clock.callback, LibDateTime:CopyDateTime(LibDateTime.currentDateTime))
			end
		)
	end
end

-- Returns new `DateTime'.
-- 
function LibDateTime:New(year, month, day, hour, minute, second)
	assert(year == nil or type(year) == "number", "Bad argument #1 to `LibDateTime:New' (number expected)")
	assert(month == nil or type(month) == "number", "Bad argument #2 to `LibDateTime:New' (number expected)")
	assert(day == nil or type(day) == "number", "Bad argument #3 to `LibDateTime:New' (number expected)")
	assert(hour == nil or type(hour) == "number", "Bad argument #4 to `LibDateTime:New' (number expected)")
	assert(minute == nil or type(minute) == "number", "Bad argument #5 to `LibDateTime:New' (number expected)")
	assert(second == nil or type(second) == "number", "Bad argument #6 to `LibDateTime:New' (number expected)")
	
	local datetime = setmetatable({}, DateTimeMetaTable)
	datetime.year   = year or 1970
	datetime.month  = month or 1
	datetime.day    = day or 1
	datetime.hour   = hour or 0
	datetime.minute = minute or 0
	datetime.second = second or 0
	
	return datetime
end

-- Returns current `DateTime'.
-- 
function LibDateTime:Now()
	if not LibDateTime:IsInitialized() then return nil end -- Returns if DateTime is not already initalized
  
	return LibDateTime:CopyDateTime(LibDateTime.currentDateTime)
end

-- Parse `datetimeString' string to extract date
--
-- Supported format:
--   YYYYMMDDHHMMSS			=> 20080912120000
--   YYYY-MM-DD HH:MM:SS	=> 2008-09-12 12:00:00
--
function LibDateTime:Parse(datetimeString)
	local _, year, month, day, hour, minute, second = nil
	
	local formats = {
		"^(%d%d%d%d)(%d%d)(%d%d)(%d%d)(%d%d)(%d%d)$",
		"^(%d%d%d%d)-(%d%d)-(%d%d) (%d%d):(%d%d):(%d%d)$",
	}
	
	 for i,format in ipairs(formats) do
		_, _, year, month, day, hour, minute, second = string.find(datetimeString, format)
		if _ then
			return LibDateTime:New(tonumber(year), tonumber(month), tonumber(day), tonumber(hour), tonumber(minute), tonumber(second))
		end
	end
	
	return nil
end

-- Returns the number of seconds since the library begin.
--
function LibDateTime:GameStartedSince()
	return LibDateTime.startedSince
end

-- Returns true if LibDateTime is initialized.
--
function LibDateTime:IsInitialized()
	if (LibDateTime.currentDateTime == nil) then return false else return true end
end

-- Returns true if `datetime`is a `DateTime' table
function LibDateTime:IsDateTime(datetime)
	return (string.find(tostring(datetime), "DateTime") ~= nil)
end

-- Returns true if `year' is a leap year.
-- 
function LibDateTime:IsLeapYear(year)
	assert(type(year) == "number", "Bad argument #1 to `LibDateTime:IsLeapYear' (number expected)")
	
	if (year % 4 == 0 and (year % 100 ~= 0 or year % 400 == 0)) then
		return true
	else
		return false
	end
end

-- Returns new DateTime equals to `datetime' argument
-- 
function LibDateTime:CopyDateTime(datetime)
	assert(LibDateTime:IsDateTime(datetime), "Bad argument #1 to `LibDateTime.CopyDateTime' (DateTime expected)")
  
  return LibDateTime:New(datetime.year, datetime.month, datetime.day, datetime.hour, datetime.minute, datetime.second)
end


-- Returns a number representing the day of the year (1..366).
--
function LibDateTime:YearDay(datetime)
	assert(LibDateTime:IsDateTime(datetime), "Bad argument #1 to `LibDateTime.YearDay' (DateTime expected)")
	
	local days = datetime.day
	if (LibDateTime:IsLeapYear(datetime.year)) then
		days = days + LibDateTime.CUMULATED_DAYS_FOR_MONTH_LEAP[datetime.month]
	else
		days = days + LibDateTime.CUMULATED_DAYS_FOR_MONTH[datetime.month]
	end
	
	return days
end

-- Returns week day number (1..7 => Mon..Sun).
--
function LibDateTime:WeekDay(datetime)
	assert(LibDateTime:IsDateTime(datetime), "Bad argument #1 to `LibDateTime.WeekDay' (DateTime expected)")
  
	local wday = (LibDateTime:YearDay(datetime) + (LibDateTime:_FirstWeekDayOfYear(datetime.year) -1)) % 7
	if wday == 0 then wday = 7 end
	
	return wday
end

-- Returns day number of first day of the `year'.
--
function LibDateTime:_FirstWeekDayOfYear(year)
	i = (year - 1) % 100
	j = (year - 1) - i
	k = i + math.floor(i / 4)
	f = 1 + ((((math.floor(j / 100) % 4) * 5) + k) % 7)
	return f
end

-- Called bye `LibDateTime:Format' to replace format string parts.
--
local datetimeToFormat
local function LibDateTimeFormatSub(match)
	
	-- %a - The abbreviated weekday name (``Sun'')
	if (match == "%a") then
		return T[LibDateTime.DAYNAMES_ABBR[LibDateTime:WeekDay(datetimeToFormat)]]
	end
	
	-- %A - The  full  weekday  name (``Sunday'')
	if (match == "%A") then
		return T[LibDateTime.DAYNAMES[LibDateTime:WeekDay(datetimeToFormat)]]
	end
	
	-- %b - The abbreviated month name (``Jan'')
	if (match == "%b") then
		return T[LibDateTime.MONTHNAMES_ABBR[datetimeToFormat.month]]
	end
	
	-- %B - The  full  month  name (``January'')
	if (match == "%B") then
		return T[LibDateTime.MONTHNAMES[datetimeToFormat.month]]
	end
	
	-- %d - Day of the month (01..31)
	if (match == "%d") then
		return string.format("%02d", datetimeToFormat.day)
	end
	
	-- %H - Hour of the day, 24-hour clock (00..23)
	if (match == "%H") then
		return string.format("%02d", datetimeToFormat.hour)
	end
	
	-- %I - Hour of the day, 12-hour clock (01..12)
	if (match == "%I") then
		return string.format("%02d", datetimeToFormat.hour % 12)
	end
	
	-- %j - Day of the year (001..366)
	if (match == "%j") then
		return string.format("%03d", LibDateTime:YearDay(datetimeToFormat))
	end
	
	-- %m - Month of the year (01..12)
	if (match == "%m") then
		return string.format("%02d", datetimeToFormat.month)
	end
	
	-- %M - Minute of the hour (00..59)
	if (match == "%M") then
		return string.format("%02d", datetimeToFormat.minute)
	end
	
	-- %p - Meridian indicator (``AM''  or  ``PM'')
	if (match == "%p") then
		if (datetimeToFormat.hour < 12) then
			return "AM"
		else
			return "PM"
		end
	end
	
	-- %S - Second of the minute (00..60)
	if (match == "%S") then
		return string.format("%02d", datetimeToFormat.second)
	end
	
	-- %w - Day of the week (Monday is 1, 1..7)
	if (match == "%w") then
		return tostring(LibDateTime:WeekDay(datetimeToFormat))
	end
	
	-- %y - Year without a century (00..99)
	if (match == "%y") then
		return string.sub(tostring(datetimeToFormat.year), -2)
	end
	
	-- %Y - Year with century
	if (match == "%Y") then
		return tostring(datetimeToFormat.year)
	end
	
	-- Literal ``%'' character
	if (match == "%%") then
		return "%"
	end
	
	return nil
end

-- Returns formatted DateTime.
-- 
-- `datetime': a `DateTime' table
-- `format': a format string
-- 
-- Code to use in format string:
--   %a - The abbreviated weekday name (``Sun'')
--   %A - The  full  weekday  name (``Sunday'')
--   %b - The abbreviated month name (``Jan'')
--   %B - The  full  month  name (``January'')
--   %d - Day of the month (01..31)
--   %H - Hour of the day, 24-hour clock (00..23)
--   %I - Hour of the day, 12-hour clock (01..12)
--   %j - Day of the year (001..366)
--   %m - Month of the year (01..12)
--   %M - Minute of the hour (00..59)
--   %p - Meridian indicator (``AM''  or  ``PM'')
--   %S - Second of the minute (00..60)
--   %w - Day of the week (Monday is 1, 1..7)
--   %y - Year without a century (00..99)
--   %Y - Year with century
--   %% - Literal ``%'' character
-- 
--   LibDateTime:Format(datetime, "%d/%m/%Y %H:%M:%S") => "12/09/2008 12:00:00"
--   LibDateTime:Format(datetime, "%Y-%m-%d %H:%M:%S") => "2008-09-19 12:00:00"
-- 
function LibDateTime:Format(datetime, format)
	assert(LibDateTime:IsDateTime(datetime), "Bad argument #1 to `LibDateTime:FormatedDateTime' (DateTime expected)")
	datetimeToFormat = datetime
	
	if (format == nil) then
		format = "%Y-%m-%d %H:%M:%S"
	else
		assert(type(format) == "string", "Bad argument #2 to `LibDateTime:FormatedDateTime' (string expected)")
	end
	
	local dateString, _ = string.gsub(format, "(%%.)", LibDateTimeFormatSub)
	return dateString
end

-- Convert a `DateTime' to unix time.
--
function LibDateTime:DateTime2UnixTime(datetime)
	assert(LibDateTime:IsDateTime(datetime), "Bad argument #1 to `LibDateTime:DateTime2UnixTime' (DateTime expected)")
	assert((datetime.year ~= nil and datetime.year >= 1970), "Bad argument #1 to `LibDateTime:DateTime2UnixTime' (DateTime is before 1970)")
	
	local timestamp = 0
	
	local year  = datetime.year or 1970
	local month  = datetime.month or 1
	local day    = datetime.day or 1
	local hour   = datetime.hour or 0
	local minute = datetime.minute or 0
	local second = datetime.second or 0
	
	-- Year
	for y = 1970,(year - 1) do
		if (LibDateTime:IsLeapYear(y)) then
			timestamp = timestamp + LibDateTime.DAYS_PER_YEAR_LEAP * LibDateTime.SECONDS_PER_DAY
		else
			timestamp = timestamp + LibDateTime.DAYS_PER_YEAR * LibDateTime.SECONDS_PER_DAY
		end
	end
	
	-- Month
	if (LibDateTime:IsLeapYear(year)) then
		timestamp = timestamp + LibDateTime.CUMULATED_DAYS_FOR_MONTH_LEAP[month] * LibDateTime.SECONDS_PER_DAY
	else
		timestamp = timestamp + LibDateTime.CUMULATED_DAYS_FOR_MONTH[month] * LibDateTime.SECONDS_PER_DAY
	end
	
	-- Day
	timestamp = timestamp + ((day - 1) * LibDateTime.SECONDS_PER_DAY)
	-- Hour
	timestamp = timestamp + (hour * LibDateTime.SECONDS_PER_HOUR)
	-- minute
	timestamp = timestamp + (minute * LibDateTime.SECONDS_PER_MINUTE)
	-- Second
	timestamp = timestamp + second
	
	return timestamp
end

-- Register a clock function to call every time update (normaly every second, but can is more)
-- when DateTime is updated. Return `handle' string if successful registered, or false if not. The
-- callback method receive a `DateTime' table in argument.
-- 
-- callback: is a function or a string (function name)
-- 
-- LibDateTime:RegisterClock(MyMod.MyClockFunction)
-- LibDateTime:RegisterClock("MyMod.MyClockFunction")
--
function LibDateTime:RegisterClock(callback)
	assert(type(callback) == "string" or type(callback) == "function", "Bad argument #1 to `LibDateTime:RegisterClock' (string or fucntion expected)")
	
	local clock = {}
	
	if (type(callback) == "string") then
		local callback_function = _G
		for x in string.gmatch(callback, "[^%.]+") do
			callback_function = callback_function[x]
			if (callback_function == nil) then return false end
		end
		clock.callback = callback_function
	else
		clock.callback = callback
	end
	
	local handle = tostring(clock)
	LibDateTime.registredClock[handle] = clock
	
	return handle
end

-- Unregister the clock responding to `handle' string (returned by `LibDateTime:RegisterClock').
-- 
function LibDateTime:UnRegisterClock(handle)
	LibDateTime.registredClock[handle] = nil
end

-- Unregister all clock.
-- 
function LibDateTime:UnRegisterAllClock()
	LibDateTime.registredClock = {}
end

-----------------------------------------------------------------------
-- Finishing touchups
if not oldminor then
  RegisterEventHandler(SystemData.Events.UPDATE_PROCESSED, "LibDateTimeOnUpdateDONOTTOUCH")
end
